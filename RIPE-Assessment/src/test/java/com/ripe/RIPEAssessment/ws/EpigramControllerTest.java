package com.ripe.RIPEAssessment.ws;

import com.ripe.RIPEAssessment.model.dto.EpigramDTO;
import com.ripe.RIPEAssessment.model.service.EpigramService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.http.HttpServletResponse;

import static com.ripe.RIPEAssessment.ws.WsTestUtil.ADD_PAGE_URL;
import static com.ripe.RIPEAssessment.ws.WsTestUtil.MAIN_PAGE_URL;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@WebMvcTest(EpigramController.class)
@ContextConfiguration(classes = {EpigramController.class})
class EpigramControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @MockBean
    private EpigramService epigramService;

    @BeforeEach
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    void whenGetMainPage_thenMainPageReturned200() throws Exception {
        when(epigramService.getRandomEpigram()).thenReturn(new EpigramDTO());

        final MvcResult mvcResult = this.mockMvc.perform(get(MAIN_PAGE_URL)).andReturn();
        assertEquals(HttpServletResponse.SC_OK, mvcResult.getResponse().getStatus());
    }

    @Test
    void whenAddEpigram_thenEpigramIsAdded() throws Exception {
        final MvcResult mvcResult = this.mockMvc.perform(post(ADD_PAGE_URL)).andReturn();
        assertEquals(HttpServletResponse.SC_FOUND, mvcResult.getResponse().getStatus());
    }
}
