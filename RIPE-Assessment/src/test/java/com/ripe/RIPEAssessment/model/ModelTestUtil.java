package com.ripe.RIPEAssessment.model;

import com.ripe.RIPEAssessment.model.dto.EpigramDTO;
import com.ripe.RIPEAssessment.model.dto.EpigramEntitiesDTO;
import com.ripe.RIPEAssessment.model.repository.EpigramEntity;
import lombok.experimental.UtilityClass;

import java.util.List;

@UtilityClass
public class ModelTestUtil {

    public static String EPIGRAM_AUTHOR = "author";

    public static String EPIGRAM_DESCRIPTION = "epigramExample";

    public static Integer EPIGRAM_ID = 1;


    public static EpigramDTO EPIGRAM_DTO = new EpigramDTO(EPIGRAM_DESCRIPTION, EPIGRAM_AUTHOR);
    public static EpigramEntity EPIGRAM_ENTITY = new EpigramEntity(EPIGRAM_ID, EPIGRAM_DESCRIPTION, EPIGRAM_AUTHOR);


    public static EpigramEntitiesDTO createEpigramDTO() {
        EpigramEntitiesDTO epigramEntitiesDTO = new EpigramEntitiesDTO();

        epigramEntitiesDTO.setEpigrams(List.of(EPIGRAM_ENTITY));
        return epigramEntitiesDTO;
    }
}
