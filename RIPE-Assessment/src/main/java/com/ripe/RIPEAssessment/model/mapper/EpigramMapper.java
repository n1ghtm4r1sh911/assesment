package com.ripe.RIPEAssessment.model.mapper;

import com.ripe.RIPEAssessment.model.dto.EpigramDTO;
import com.ripe.RIPEAssessment.model.repository.EpigramEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface EpigramMapper {

    @Mapping(target = "description", source = "epigram")
    EpigramDTO asEpigramDTO(EpigramEntity epigramEntity);

}
