package com.ripe.RIPEAssessment.model.service;

import com.ripe.RIPEAssessment.model.dto.EpigramDTO;

import java.io.IOException;

public interface EpigramService {
    EpigramDTO getRandomEpigram() throws IOException;

    void addEpigram(EpigramDTO epigramDTO) throws IOException;
}
