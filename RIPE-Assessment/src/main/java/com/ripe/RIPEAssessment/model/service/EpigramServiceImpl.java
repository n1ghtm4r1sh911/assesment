package com.ripe.RIPEAssessment.model.service;

import com.ripe.RIPEAssessment.model.dto.EpigramDTO;
import com.ripe.RIPEAssessment.model.dto.EpigramEntitiesDTO;
import com.ripe.RIPEAssessment.model.mapper.EpigramMapper;
import com.ripe.RIPEAssessment.model.repository.EpigramEntity;
import com.ripe.RIPEAssessment.model.repository.EpigramRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Service
public class EpigramServiceImpl implements EpigramService {

    @Autowired
    private EpigramRepository epigramRepository;

    @Autowired
    private EpigramMapper epigramMapper;

    @Override
    public EpigramDTO getRandomEpigram() throws IOException {
        EpigramEntitiesDTO epigramEntitiesDTO = epigramRepository.findEpigrams();
        List<EpigramEntity> availableEpigrams = epigramEntitiesDTO.getEpigrams();
        int randomValue = (int) ((Math.random() * (availableEpigrams.size() - 1)) + 1);

        Optional<EpigramEntity> epigramEntity = availableEpigrams.stream().filter(epigram -> epigram.getId().equals(randomValue)).findFirst();
        return epigramEntity.map(epigramMapper::asEpigramDTO).orElse(null);
    }

    @Override
    public void addEpigram(EpigramDTO epigramDTO) throws IOException {
        epigramRepository.addNewEpigram(epigramDTO);
    }

}
