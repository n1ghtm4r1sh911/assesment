package com.ripe.RIPEAssessment.ws;

import lombok.experimental.UtilityClass;

@UtilityClass
public class WsTestUtil {

    public static String ADD_PAGE_URL = "/addEpigram";
    public static String MAIN_PAGE_URL = "/mainPage";
}
