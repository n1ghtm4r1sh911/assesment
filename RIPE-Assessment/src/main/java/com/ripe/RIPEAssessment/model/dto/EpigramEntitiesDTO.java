package com.ripe.RIPEAssessment.model.dto;

import com.ripe.RIPEAssessment.model.repository.EpigramEntity;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class EpigramEntitiesDTO {
    private List<EpigramEntity> epigrams = new ArrayList<>();
}
