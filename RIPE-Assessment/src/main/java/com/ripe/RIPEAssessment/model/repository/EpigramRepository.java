package com.ripe.RIPEAssessment.model.repository;

import com.ripe.RIPEAssessment.model.dto.EpigramDTO;
import com.ripe.RIPEAssessment.model.dto.EpigramEntitiesDTO;

import java.io.IOException;

public interface EpigramRepository {
    EpigramEntitiesDTO findEpigrams() throws IOException;

    void addNewEpigram(EpigramDTO epigramDTO) throws IOException;
}
