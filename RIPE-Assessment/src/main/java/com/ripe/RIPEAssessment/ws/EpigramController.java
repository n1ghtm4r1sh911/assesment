package com.ripe.RIPEAssessment.ws;

import com.ripe.RIPEAssessment.model.dto.EpigramDTO;
import com.ripe.RIPEAssessment.model.service.EpigramService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.IOException;

@Controller
public class EpigramController {

    @Autowired
    private EpigramService epigramService;

    @RequestMapping(path = "/mainPage")
    public String mainPage(Model model) throws IOException {
        EpigramDTO epigramDTO = epigramService.getRandomEpigram();
        model.addAttribute("epigram", epigramDTO.getDescription());
        model.addAttribute("author", epigramDTO.getAuthor());

        return "main-page";
    }

    @PostMapping(path = "/addEpigram")
    public String addEpigram(EpigramDTO epigramDTO) throws IOException {
        epigramService.addEpigram(epigramDTO);
        return "redirect:/mainPage";
    }
}
