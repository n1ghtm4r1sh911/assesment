package com.ripe.RIPEAssessment.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EpigramDTO {

    private String description;

    private String author;
}
