package com.ripe.RIPEAssessment.model.repository;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class EpigramEntity {

    private Integer id;

    private String epigram;

    private String author;
}
