package com.ripe.RIPEAssessment.model.repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.dataformat.yaml.YAMLGenerator;
import com.ripe.RIPEAssessment.model.dto.EpigramDTO;
import com.ripe.RIPEAssessment.model.dto.EpigramEntitiesDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.io.File;
import java.io.IOException;

@Repository
public class EpigramRepositoryImpl implements EpigramRepository {

    @Value("classpath:mongodb/epigram.yml")
    private File epigramFile;

    @Override
    public EpigramEntitiesDTO findEpigrams() throws IOException {
        return getEpigramEntitiesDTO();
    }


    @Override
    public void addNewEpigram(EpigramDTO epigramDTO) throws IOException {
        EpigramEntitiesDTO epigramEntities = getEpigramEntitiesDTO();
        int firstFreeIdentifier = epigramEntities.getEpigrams().size() + 1;
        epigramEntities.getEpigrams().add(new EpigramEntity(firstFreeIdentifier, epigramDTO.getDescription(), epigramDTO.getAuthor()));

        ObjectMapper mapper = new ObjectMapper(new YAMLFactory().disable(YAMLGenerator.Feature.WRITE_DOC_START_MARKER));
        mapper.writeValue(epigramFile, epigramEntities);
    }

    private EpigramEntitiesDTO getEpigramEntitiesDTO() throws IOException {
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        mapper.findAndRegisterModules();
        return mapper.readValue(epigramFile, EpigramEntitiesDTO.class);
    }
}
