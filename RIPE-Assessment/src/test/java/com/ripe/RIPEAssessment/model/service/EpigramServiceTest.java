package com.ripe.RIPEAssessment.model.service;

import com.ripe.RIPEAssessment.model.dto.EpigramDTO;
import com.ripe.RIPEAssessment.model.mapper.EpigramMapper;
import com.ripe.RIPEAssessment.model.mapper.EpigramMapperImpl;
import com.ripe.RIPEAssessment.model.repository.EpigramRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;

import static com.ripe.RIPEAssessment.model.ModelTestUtil.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class EpigramServiceTest {

    @Mock
    private EpigramRepository epigramRepository;

    @Spy
    private EpigramMapper epigramMapper = new EpigramMapperImpl();

    @InjectMocks
    private EpigramServiceImpl epigramService;

    @Test
    void whenGetRandomEpigram_thenRandomEpigram() throws IOException {
        when(epigramRepository.findEpigrams()).thenReturn(createEpigramDTO());

        EpigramDTO epigramDTO = epigramService.getRandomEpigram();
        assertNotNull(epigramDTO);
        assertEquals(EPIGRAM_DESCRIPTION, epigramDTO.getDescription());
        assertEquals(EPIGRAM_AUTHOR, epigramDTO.getAuthor());
    }


    @Test
    void whenGetRandomEpigram_thenError() throws IOException {
        doThrow(IOException.class).when(epigramRepository).findEpigrams();
        assertThrows(IOException.class, () -> epigramService.getRandomEpigram());
    }

    @Test
    void whenAddEpigram_thenEpigramAdded() throws IOException {
        epigramService.addEpigram(EPIGRAM_DTO);
        verify(epigramRepository, timeout(1)).addNewEpigram(EPIGRAM_DTO);
    }

    
    @Test
    void whenAddEpigram_thenError() throws IOException {
        doThrow(IOException.class).when(epigramRepository).addNewEpigram(EPIGRAM_DTO);
        assertThrows(IOException.class, () -> epigramService.addEpigram(EPIGRAM_DTO));
    }
}
